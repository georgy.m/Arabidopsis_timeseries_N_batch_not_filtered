# Directory map
`main.py` - reproduction script;


`TMM_TGR_round.txt` - TMM-normalized gene expression across different time frames;


`output` - figures & and datatables;


`output/tails` - KDE plots for logfoldchange neighbor statistic for each time frame with number of genes in both tails;


`output/foldchanges_rel.csv`- datatable with logfoldchange statistics as computed by the reproduction script;


`output/tail_counts.csv` - datable with a number of genes in left and right tails of the logfoldchange statistic distribution;


`output/violin.png` - violin plots for N-batches (colored in red) together with the closest non-N-batch (coloured in blue);


`output/barplot.png` - barplot with average logfoldchanges across neighbours for each time frame. N-batches are coloured in red;


`output/marginals_scatter.png` - huge 5-columns plot. First two plots are KDEs of an N-batch and it's 40-minute behind&ahead neighbours, for both centered and non-centered statistics (though they appear to be very similar). Note that the 40-minute gap was chosen to ensure that logfoldchange statistics don't overlap with an N-batch. The last 3 plots are scatterplots build between those 3 logfoldchange statistics;


`output/fraction.png` - plotted fraction of genes in tails conditioned on a "definition" of tail: at x-axis, is a cutoff-value - we say that eveyrthing left and right from the negative cutoff values and positive cutoff value is a tail;

# Reproduction instructions

Obtained plots and tables are exactly reproducible via evaluating the `main.py` script. It is just as easy as

> python3 main.py


You can rename the `output` directory beforehand to see that produced results are the same.

# Details

- The statistic we used for the analysis is computed for each timeframe/"batch" as an average logfoldchange to its neighbours (i.e. points that stay off for 20 minutes);

- We filtered "outlier"-like genes for most plots and dataframes (with an exception for tails-related output, i.e. figures in `tails` and `tail_counts.csv`) using IQR (inter-quantile range) with factor 4. We did it entirely for better visuals. This behavior can be altered by either configuring a custom factor by chaing the line `iqr_multiplier = 4` in the `main.py`, or be disabled alltogether via setting the flag `drop_outliers` to `False`;

- For building `tail_counts.csv` and plots in `output/tails`, we assumed logfoldchange values to lie in tails if they fall either below `tail_low` (left tail) or above `tail_high` (right tail) cutoff-values that are changeable at the beginning of the reproduction script;

- When plotting `fraction.png`, we counted union of genes in tails across time frames. There are in total 23 time frames that keep away from N-frames for at least 40 minutes, and 12 N-frames in total. As a number of non-N-batch time frames is almost 2 times greater, there is a higher chance to see more genes in it by random. Hence, we included only odd time frames in non-N-batches, resulting in equal number of time frames in both N-batches and non-N-batches. `fraction_all.png` has all 23 time frames in non-N-batches and can be produced by setting the flag `fractions_all` to `True`.

# Results

Here we show some figures from the output folder. Notice that their resolution is not maxed here and you can open them directly for inspection.

## Violin plots

![Violin plot](./output/violin.png)

## Bar plot

![Bar plot](./output/barplot.png)

## Fraction plot

![Fraction plot](./output/fraction.png)


## KDE marginals & scatter plots
![KDE& scatter plots](./output/marginals_scatter.png)

## Tail counts
We don't show figures for each of the 70 statistics - you can find them in `output/tails` folder. See an example:
![N tail count at 01:00 (an N-batch)](./output/tails/01_00N.png)
![N tail count at 01:40 ](./output/tails/01_40.png)



